﻿using UnityEngine;
using System.Collections;

public class ConnectionGUI : MonoBehaviour
{
    string remoteIP = "127.0.0.1";
    int remotePort = 25000;
    int listenPort = 25000;
    bool useNAT = false;
    string yourIP = "";
    string yourPort = "";

    string ipaddress = "";
    string port = "";

    void OnGUI()
    {
        if (Network.peerType == NetworkPeerType.Disconnected)
        {
            if (GUI.Button(new Rect(10, 10, 100, 30), "Connect"))
            {
                Network.useNat = useNAT;

                Network.Connect(remoteIP, remotePort);
            }
            if (GUI.Button(new Rect(10, 50, 100, 30), "Start Server"))
            {
                Network.useNat = useNAT;
                Network.InitializeServer(32, listenPort);
                foreach (GameObject go in FindObjectsOfType(typeof(GameObject)))
                {
                    go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);
                }
            }

            remoteIP = GUI.TextField(new Rect(120, 10, 100, 20), remoteIP);
            remotePort = int.Parse(GUI.TextField(new Rect(230, 10, 40, 20), remotePort.ToString()));
        }
        else
        {
            ipaddress = Network.player.ipAddress;
            port = Network.player.port.ToString();
            GUI.Label(new Rect(140, 20, 250, 40), "IP Adress: " + ipaddress + ":" + port);

            if (GUI.Button(new Rect(10, 10, 100, 50), "Diconnect"))
            {
                Network.Disconnect(200);
            }
        }
    }
    
    void OnConnectedToServer()
    {
        foreach (var go in (GameObject[])FindObjectsOfType(typeof(GameObject)))
        {
            go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);
        }
    }

    void OnNetworkLoadedLevel()
    {
        Debug.Log("oi");
    }
}
